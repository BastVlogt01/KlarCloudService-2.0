/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.serverprocess.setup;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.utility.files.DownloadManager;
import jline.console.ConsoleReader;

import java.io.IOException;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

public class SpigotVersionSetup {
    private final ConsoleReader consoleReader;
    private static final String PATH = "klarcloud/files/spigot.jar";

    /**
     * Starts the setup of the MinecraftServerVersion
     *
     * @param consoleReader
     */
    public SpigotVersionSetup(final ConsoleReader consoleReader) {
        this.consoleReader = consoleReader;

        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().emptyLine().info("Please choose a SpigotServer Version");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"Spigot\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"Paper\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"Taco\"");

        String version = "";
        while (version.equals("")) {
            try {
                version = this.consoleReader.readLine();

                switch (version.toLowerCase()) {
                    case "spigot":
                        this.setupSpigotVersion();
                        break;
                    case "paper":
                        this.setupPaperVersion();
                        break;
                    case "taco":
                        this.setupTacoVersion();
                        break;
                    default:
                        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().warn("This SpigotServer-Version is not supported, yet");
                        version = "";
                        break;
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Setup of SpigotVersions
     */
    private void setupSpigotVersion() {
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().emptyLine().info("Please choose a MinecraftServer Version");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.8.8\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.10.2\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.11.2\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.12.2\"");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.13.2\"");

        String version = "";
        while (version.equals("")) {
            try {
                version = this.consoleReader.readLine();

                switch (version.toLowerCase()) {
                    case "1.8.8":
                        DownloadManager.download("Spigot[/1.8.8]", "https://cdn.getbukkit.org/spigot/spigot-1.8.8-R0.1-SNAPSHOT-latest.jar", PATH);
                        break;
                    case "1.10.2":
                        DownloadManager.download("Spigot[/1.10.2]", "https://cdn.getbukkit.org/spigot/spigot-1.10.2-R0.1-SNAPSHOT-latest.jar", PATH);
                        break;
                    case "1.11.2":
                        DownloadManager.download("Spigot[/1.11.2]", "https://cdn.getbukkit.org/spigot/spigot-1.11.2.jar", PATH);
                        break;
                    case "1.12.2":
                        DownloadManager.download("Spigot[/1.12.2]", "https://cdn.getbukkit.org/spigot/spigot-1.12.2.jar", PATH);
                        break;
                    case "1.13.2":
                        DownloadManager.download("Spigot[/1.13.2]", "https://cdn.getbukkit.org/spigot/spigot-1.13.2.jar", PATH);
                        break;
                    default:
                        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().warn("This MinecraftServer-Version is not supported, yet");
                        version = "";
                        break;
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Setup of PaperVersions
     */
    private void setupPaperVersion() {
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().emptyLine().info("Please choose a MinecraftServer Version");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.8.8\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.11.2\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.12.2\"");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.13.2\"");

        String version = "";
        while (version.equals("")) {
            try {
                version = this.consoleReader.readLine();

                switch (version.toLowerCase()) {
                    case "1.8.8":
                        DownloadManager.download("Paper[/1.8.8]", "https://papermc.io/ci/job/Paper/443/artifact/Paperclip.jar", PATH);
                        break;
                    case "1.11.2":
                        DownloadManager.download("Paper[/1.11.2]", "https://papermc.io/ci/job/Paper/1104/artifact/paperclip.jar", PATH);
                        break;
                    case "1.12.2":
                        DownloadManager.download("Paper[/1.12.2]", "https://papermc.io/ci/job/Paper/lastSuccessfulBuild/artifact/paperclip.jar", PATH);
                        break;
                    case "1.13.2":
                        DownloadManager.download("Paper[/1.13.2]", "https://papermc.io/ci/job/Paper-1.13/lastSuccessfulBuild/artifact/paperclip.jar", PATH);
                        break;
                    default:
                        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().warn("This MinecraftServer-Version is not supported, yet");
                        version = "";
                        break;
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Setup of TacoVersions
     */
    private void setupTacoVersion() {
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().emptyLine().info("Please choose a MinecraftServer Version");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.8.8\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.11.2\",");
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("   \"1.12.2\"");

        String version = "";
        while (version.equals("")) {
            try {
                version = this.consoleReader.readLine();

                switch (version.toLowerCase()) {
                    case "1.8.8":
                        DownloadManager.download("Taco[/1.8.8]", "https://yivesmirror.com/grab/tacospigot/TacoSpigot-1.8.8", PATH);
                        break;
                    case "1.11.2":
                        DownloadManager.download("Taco[/1.11.2]", "https://yivesmirror.com/grab/tacospigot/TacoSpigot-1.11.2-b102", PATH);
                        break;
                    case "1.12.2":
                        DownloadManager.download("Taco[/1.12.2]", "https://yivesmirror.com/grab/tacospigot/TacoSpigot-1.12.2-b114", PATH);
                        break;
                    default:
                        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().warn("This MinecraftServer-Version is not supported, yet");
                        version = "";
                        break;
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
