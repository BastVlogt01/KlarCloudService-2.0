/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Collections;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

public final class PacketOutSendControllerConsoleMessage extends Packet {
    public PacketOutSendControllerConsoleMessage(final String message) {
        super("SendControllerConsoleMessage", new Configuration().addStringProperty("message", message), Collections.singletonList(QueryType.COMPLETE), PacketSender.CLIENT);
    }

    public PacketOutSendControllerConsoleMessage(final String... messages) {
        for (String message : messages)
            KlarCloudClient.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutSendControllerConsoleMessage(message));
    }
}
