/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.meta.startup.ProxyStartupInfo;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 29.10.2018
 */

public class PacketInStartProxy implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        final ProxyGroup proxyGroup = configuration.getValue("group", TypeTokenAdaptor.getProxyGroupType());

        KlarCloudClient.getInstance().getCloudProcessStartupHandler().offerProxyProcess(new ProxyStartupInfo(
                configuration.getValue("proxyProcess", UUID.class), configuration.getStringValue("name"), proxyGroup, configuration.getConfiguration("preConfig"), configuration.getIntegerValue("id")
        ));

        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("ProxyProcess " + configuration.getStringValue("name") + " has been successfully added to the Client queue");
    }
}
