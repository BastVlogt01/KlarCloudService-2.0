/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.serverprocess.startup.CloudServerStartupHandler;
import de.klarcloudservice.serverprocess.startup.ProxyStartupHandler;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 09.12.2018
 */

public class PacketInStopProcess implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        if (KlarCloudClient.getInstance().getCloudProcessScreenService().getRegisteredServerHandler(configuration.getStringValue("name")) != null) {
            final CloudServerStartupHandler cloudServerStartupHandler = KlarCloudClient.getInstance().getCloudProcessScreenService().getRegisteredServerHandler(configuration.getStringValue("name"));
            cloudServerStartupHandler.shutdown();
            KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("Trying to shutdown Process [Name=" + cloudServerStartupHandler.getServerStartupInfo().getName() + "]...");
        } else if (KlarCloudClient.getInstance().getCloudProcessScreenService().getRegisteredProxyHandler(configuration.getStringValue("name")) != null) {
            final ProxyStartupHandler proxyStartupHandler = KlarCloudClient.getInstance().getCloudProcessScreenService().getRegisteredProxyHandler(configuration.getStringValue("name"));
            proxyStartupHandler.shutdown(null);
            KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("Trying to shutdown Process [Name=" + proxyStartupHandler.getProxyStartupInfo().getName() + "]...");
        }
    }
}
