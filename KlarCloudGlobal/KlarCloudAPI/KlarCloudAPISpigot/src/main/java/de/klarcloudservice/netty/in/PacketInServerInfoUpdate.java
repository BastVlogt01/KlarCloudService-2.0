/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudAPISpigot;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.bootstrap.SpigotBootstrap;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.internal.events.CloudServerInfoUpdateEvent;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.TypeTokenAdaptor;
import de.klarcloudservice.utility.cloudsystem.InternalCloudNetwork;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 12.12.2018
 */

public class PacketInServerInfoUpdate implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        final ServerInfo serverInfo = configuration.getValue("serverInfo", TypeTokenAdaptor.getServerInfoType());
        final InternalCloudNetwork internalCloudNetwork = configuration.getValue("networkProperties", TypeTokenAdaptor.getInternalCloudNetworkType());

        KlarCloudAPISpigot.getInstance().setInternalCloudNetwork(internalCloudNetwork);
        KlarCloudLibraryService.getInstance().setInternalCloudNetwork(internalCloudNetwork);

        if (serverInfo.getCloudProcess().getName().equals(KlarCloudAPISpigot.getInstance().getServerInfo().getCloudProcess().getName()))
            KlarCloudAPISpigot.getInstance().setServerInfo(serverInfo);

        SpigotBootstrap.getInstance().getServer().getPluginManager().callEvent(new CloudServerInfoUpdateEvent(serverInfo));
    }
}
