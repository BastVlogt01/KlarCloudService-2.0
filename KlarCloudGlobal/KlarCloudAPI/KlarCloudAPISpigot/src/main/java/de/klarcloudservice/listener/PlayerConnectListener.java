/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.listener;

import de.klarcloudservice.KlarCloudAPISpigot;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.bootstrap.SpigotBootstrap;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.enums.GroupResetType;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.netty.packets.PacketOutCheckPlayer;
import de.klarcloudservice.netty.packets.PacketOutServerInfoUpdate;
import de.klarcloudservice.netty.packets.PacketOutStartGameServer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 09.12.2018
 */

public class PlayerConnectListener implements Listener {
    private boolean isAllowAutoStart = true;

    @EventHandler(priority = EventPriority.LOW)
    public void handle(final AsyncPlayerPreLoginEvent event) {
        KlarCloudAPISpigot.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutCheckPlayer(event.getUniqueId()));
        KlarCloudLibrary.sleep(25);
        if (!SpigotBootstrap.getInstance().getAcceptedPlayers().contains(event.getUniqueId())) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, KlarCloudAPISpigot.getInstance().getInternalCloudNetwork().getMessage("internal-api-spigot-connect-only-proxy"));
            return;
        }

        if (KlarCloudAPISpigot.getInstance().getServerInfo().getServerGroup().isMaintenance()
                && SpigotBootstrap.getInstance().getServer().getPlayer(event.getUniqueId()) != null
                && !SpigotBootstrap.getInstance().getServer().getPlayer(event.getUniqueId()).hasPermission("klarcloud.join.server.maintenance")) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, KlarCloudAPISpigot.getInstance().getInternalCloudNetwork().getMessage("internal-api-spigot-connect-no-permission"));
            return;
        }

        final ServerInfo serverInfo = KlarCloudAPISpigot.getInstance().getServerInfo();
        List<UUID> online = serverInfo.getOnlinePlayers();
        online.add(event.getUniqueId());
        serverInfo.setOnlinePlayers(online);

        KlarCloudAPISpigot.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutServerInfoUpdate(serverInfo));

        event.allow();
    }

    @EventHandler(priority = EventPriority.LOW)
    public void handle(final PlayerJoinEvent event) {
        if (this.getOnlinePlayerPercent() >= KlarCloudAPISpigot.getInstance().getServerInfo().getServerGroup().getPercentOfOnline()
                && KlarCloudAPISpigot.getInstance().getServerInfo().getServerGroup().getPercentOfOnline() > 0
                && !KlarCloudAPISpigot.getInstance().getServerInfo().getServerGroup().getGroupResetType().equals(GroupResetType.NO_RESET)
                && this.isAllowAutoStart) {
            KlarCloudAPISpigot.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new PacketOutStartGameServer(KlarCloudAPISpigot.getInstance().getServerInfo().getServerGroup(), new Configuration()));
            this.isAllowAutoStart = false;

            SpigotBootstrap.getInstance().getServer().getScheduler().runTaskLaterAsynchronously(SpigotBootstrap.getInstance(), () -> {
                this.isAllowAutoStart = true;
            }, 8000L);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void handle(final PlayerQuitEvent event) {
        final ServerInfo serverInfo = KlarCloudAPISpigot.getInstance().getServerInfo();
        List<UUID> online = serverInfo.getOnlinePlayers();
        online.remove(event.getPlayer().getUniqueId());
        serverInfo.setOnlinePlayers(online);

        KlarCloudAPISpigot.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutServerInfoUpdate(serverInfo));
    }

    private double getOnlinePlayerPercent() {
        return (((double) SpigotBootstrap.getInstance().getServer().getOnlinePlayers().size()) / (double) KlarCloudAPISpigot.getInstance().getServerInfo().getServerGroup().getAdvancedConfiguration().getMaxPlayers()) * 100;
    }
}
