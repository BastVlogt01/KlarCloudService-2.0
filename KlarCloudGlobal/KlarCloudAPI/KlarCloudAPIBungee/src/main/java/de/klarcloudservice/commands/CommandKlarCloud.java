/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.netty.packets.PacketOutDispatchConsoleCommand;
import de.klarcloudservice.utility.StringUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.util.Arrays;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

public final class CommandKlarCloud extends Command {
    public CommandKlarCloud() {
        super("klarcloud");
    }

    @Override
    public final String getPermission() {
        return "klarcloud.command.klarcloud";
    }

    @Override
    public final String[] getAliases() {
        return new String[]{"kc", "klarc", "kcloud"};
    }

    @Override
    public void execute(final CommandSender commandSender, final String[] strings) {
        if (!commandSender.hasPermission("klarcloud.command.klarcloud")) {
            commandSender.sendMessage(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-command-no-permission")));
            return;
        }

        if (strings.length == 0) {
            commandSender.sendMessage(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-command-klarcloud-invalid-syntax")));
            return;
        }

        if (strings[0].equalsIgnoreCase("exit") && !commandSender.hasPermission("klarcloud.command.klarcloud.exit")) {
            commandSender.sendMessage(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-command-klarcloud-no-exit-permission")));
            return;
        }

        StringBuilder stringBuilder = new StringBuilder();
        Arrays.stream(strings).forEach(e -> stringBuilder.append(e).append(StringUtil.SPACE));

        KlarCloudAPIBungee.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new PacketOutDispatchConsoleCommand(stringBuilder.substring(0, stringBuilder.length() - 1)));
        commandSender.sendMessage(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-command-klarcloud-command-success")));
    }
}
