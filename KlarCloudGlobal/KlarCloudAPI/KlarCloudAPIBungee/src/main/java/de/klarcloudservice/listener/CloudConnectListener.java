/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.listener;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.netty.packets.PacketOutLoginPlayer;
import de.klarcloudservice.netty.packets.PacketOutLogoutPlayer;
import de.klarcloudservice.netty.packets.PacketOutProxyInfoUpdate;
import de.klarcloudservice.netty.packets.PacketOutSendControllerConsoleMessage;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * @author _Klaro | Pasqual K. / created on 03.11.2018
 */

public final class CloudConnectListener implements Listener {
    @EventHandler(priority = -127)
    public void handle(final ServerConnectEvent event) {
        if (event.getPlayer().getServer() == null) {
            final ServerInfo serverInfo = KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getServerProcessManager().nextFreeLobby(event.getPlayer().getPermissions());
            if (serverInfo != null)
                event.setTarget(ProxyServer.getInstance().getServerInfo(serverInfo.getCloudProcess().getName()));
            else
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = -128)
    public void handle(final LoginEvent event) {
        if (KlarCloudAPIBungee.getInstance().getProxyInfo().getProxyGroup().getProxyConfig().isMaintenance()
                && ProxyServer.getInstance().getPlayer(event.getConnection().getUniqueId()) != null
                && !ProxyServer.getInstance().getPlayer(event.getConnection().getUniqueId()).hasPermission("klarcloud.join.maintenance")) {
            event.setCancelled(true);
            event.setCancelReason(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-maintenance-join-no-permission")));
            return;
        } else if (KlarCloudAPIBungee.getInstance().getProxyInfo().getProxyGroup().getProxyConfig().isMaintenance()
                && !KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getProxyGroups().get(KlarCloudAPIBungee.getInstance().getProxyInfo().getProxyGroup().getName()).getProxyConfig().getWhitelist().contains(event.getConnection().getName())) {
            event.setCancelled(true);
            event.setCancelReason(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-maintenance-join-no-permission")));
            return;
        }

        event.setCancelled(false);
        KlarCloudAPIBungee.getInstance().getProxyInfo().getOnlinePlayers().add(event.getConnection().getUniqueId());
        KlarCloudAPIBungee.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new PacketOutLoginPlayer(event.getConnection().getUniqueId()), new PacketOutProxyInfoUpdate(KlarCloudAPIBungee.getInstance().getProxyInfo()), new PacketOutSendControllerConsoleMessage("Player [Name=" + event.getConnection().getName() + "/UUID=" + event.getConnection().getUniqueId() + "/IP=" + event.getConnection().getAddress().getAddress().getHostAddress() + "] is now connected"));
    }

    @EventHandler(priority = -128)
    public void handle(final PlayerDisconnectEvent event) {
        KlarCloudAPIBungee.getInstance().getProxyInfo().getOnlinePlayers().remove(event.getPlayer().getUniqueId());
        KlarCloudAPIBungee.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new PacketOutLogoutPlayer(event.getPlayer().getUniqueId()), new PacketOutProxyInfoUpdate(KlarCloudAPIBungee.getInstance().getProxyInfo()), new PacketOutSendControllerConsoleMessage("Player [Name=" + event.getPlayer().getName() + "/UUID=" + event.getPlayer().getUniqueId() + "/IP=" + event.getPlayer().getAddress().getAddress().getHostAddress() + "] is now disconnected"));
    }

    @EventHandler(priority = -127)
    public void handle(final ServerKickEvent event) {
        final ServerInfo serverInfo = KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getServerProcessManager().nextFreeLobby(event.getPlayer().getPermissions());
        if (serverInfo != null) {
            event.setCancelled(true);
            event.setCancelServer(ProxyServer.getInstance().getServerInfo(serverInfo.getCloudProcess().getName()));
        } else {
            event.setCancelled(false);
            event.setCancelServer(null);
            event.getPlayer().sendMessage(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-connect-hub-no-server")));
        }
    }
}
