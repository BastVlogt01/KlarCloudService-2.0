/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.bootstrap.BungeecordBootstrap;
import de.klarcloudservice.meta.enums.GroupResetType;
import de.klarcloudservice.meta.info.ServerInfo;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * @author _Klaro | Pasqual K. / created on 11.12.2018
 */

public class CommandHub extends Command {
    public CommandHub() {
        super("hub");
    }

    @Override
    public String[] getAliases() {
        return new String[]{"l", "lobby", "leave"};
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!(commandSender instanceof ProxiedPlayer)) return;

        final ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        if (KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredServerByName(proxiedPlayer.getServer().getInfo().getName()) != null
                && KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredServerByName(proxiedPlayer.getServer().getInfo().getName()).getServerGroup().getGroupResetType().equals(GroupResetType.LOBBY)) {
            proxiedPlayer.sendMessage(ChatMessageType.CHAT, TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-command-hub-already")));
            return;
        }

        final ServerInfo fallback = KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getServerProcessManager().nextFreeLobby(proxiedPlayer.getPermissions());
        if (fallback == null)
            proxiedPlayer.sendMessage(ChatMessageType.CHAT, TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-command-hub-not-available")));
        else
            proxiedPlayer.connect(BungeecordBootstrap.getInstance().getProxy().getServerInfo(fallback.getCloudProcess().getName()));
    }
}
