/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.listener;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.bootstrap.BungeecordBootstrap;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.utility.StringUtil;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

/**
 * @author _Klaro | Pasqual K. / created on 02.11.2018
 */

public final class CloudProxyPingListener implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(final ProxyPingEvent event) {
        final ProxyGroup proxyGroup = KlarCloudAPIBungee.getInstance().getProxyStartupInfo().getProxyGroup();
        if (proxyGroup == null) return;

        if (proxyGroup.getProxyConfig().getNormalLayout().isEnabled()) {
            event.getResponse().setDescriptionComponent(new TextComponent(TextComponent.fromLegacyText(
                    ChatColor.translateAlternateColorCodes('&', proxyGroup.getProxyConfig().getNormalLayout().getLineOne() + "\n"
                            + proxyGroup.getProxyConfig().getNormalLayout().getLineTwo())
                            .replace("%version%", StringUtil.KLARCLOUD_VERSION + "@" + StringUtil.KLARCLOUD_SPECIFICATION)
                            .replace("%proxy%", KlarCloudAPIBungee.getInstance().getProxyInfo().getCloudProcess().getName())
            )));
        }

        event.getResponse().setPlayers(new ServerPing.Players(
                proxyGroup.getProxyConfig().getMaxPlayers(),
                BungeecordBootstrap.getInstance().getProxy().getPlayers().size(),
                event.getResponse().getPlayers().getSample()
        ));

        event.setResponse(event.getResponse());
    }
}
