/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.addons.JavaAddon;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.event.Listener;

import java.util.Arrays;

/**
 * @author _Klaro | Pasqual K. / created on 11.12.2018
 */

/**
 * Class for Controller Addons
 */
public class ControllerAddonImpl extends JavaAddon<KlarCloudController> {
    @Override
    public KlarCloudController getInternalKlarCloudSystem() {
        return KlarCloudController.getInstance();
    }

    @Override
    public KlarCloudLibraryService getInternalKlarCloudLibrary() {
        return KlarCloudLibraryService.getInstance();
    }

    public void registerCommand(final String name, final Command command) {
        KlarCloudController.getInstance().getCommandManager().registerCommand(name, command);
    }

    public void registerCommand(final String name, final Command command, final String... aliases) {
        KlarCloudController.getInstance().getCommandManager().registerCommand(name, command, aliases);
    }

    public void registerListener(final Listener listener) {
        KlarCloudController.getInstance().getEventManager().registerListener(listener);
    }

    public void registerListener(final Listener... listeners) {
        Arrays.stream(listeners).forEach(e -> KlarCloudController.getInstance().getEventManager().registerListener(e));
    }
}
