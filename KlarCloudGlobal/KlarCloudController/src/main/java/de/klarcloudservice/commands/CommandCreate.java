/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;
import de.klarcloudservice.meta.client.Client;
import de.klarcloudservice.meta.proxy.defaults.DefaultProxyGroup;
import de.klarcloudservice.meta.server.defaults.DefaultGroup;

/**
 * @author _Klaro | Pasqual K. / created on 09.12.2018
 */

public final class CommandCreate implements Command {
    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        if (args.length < 2) {
            commandSender.sendMessage("create CLIENT <name> <ip>");
            commandSender.sendMessage("create SERVERGROUP <name> <client>");
            commandSender.sendMessage("create PROXYGROUP <name> <client> <host>");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "client": {
                if (args.length != 3) {
                    commandSender.sendMessage("create CLIENT <name> <ip>");
                    return;
                }

                if (KlarCloudController.getInstance().getInternalCloudNetwork().getClients().get(args[1]) != null) {
                    commandSender.sendMessage("Client already exists.");
                    return;
                }

                commandSender.sendMessage("Trying to create new Client...");
                KlarCloudController.getInstance().getCloudConfiguration().createClient(new Client(args[1], args[2]));
                break;
            }
            case "servergroup": {
                if (args.length != 3) {
                    commandSender.sendMessage("create SERVERGROUP <name> <client>");
                    return;
                }

                if (KlarCloudController.getInstance().getInternalCloudNetwork().getServerGroups().get(args[1]) != null) {
                    commandSender.sendMessage("ServerGroup already exists.");
                    return;
                }

                commandSender.sendMessage("Trying to create new ServerGroup...");
                KlarCloudController.getInstance().getCloudConfiguration().createServerGroup(new DefaultGroup(args[1], args[2]));
                break;
            }
            case "proxygroup": {
                if (args.length != 4) {
                    commandSender.sendMessage("create PROXYGROUP <name> <client> <host>");
                    return;
                }

                if (KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().get(args[1]) != null) {
                    commandSender.sendMessage("ProxyGroup already exists.");
                    return;
                }

                commandSender.sendMessage("Trying to create new ProxyGroup...");
                KlarCloudController.getInstance().getCloudConfiguration().createProxyGroup(new DefaultProxyGroup(args[1], args[2], args[3]));
                break;
            }
            default:
                commandSender.sendMessage("create CLIENT <name> <ip>");
                commandSender.sendMessage("create SERVERGROUP <name> <client>");
                commandSender.sendMessage("create PROXYGROUP <name> <client> <host>");
        }
    }

    @Override
    public String getPermission() {
        return "klarcloud.command.create";
    }
}
