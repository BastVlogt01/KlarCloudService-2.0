/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.netty.packets.PacketOutStartGameServer;
import de.klarcloudservice.netty.packets.PacketOutStartProxy;
import de.klarcloudservice.netty.packets.PacketOutStopProcess;

import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 09.12.2018
 */

public final class CommandProcess implements Command {
    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        if (args.length < 1) {
            commandSender.sendMessage("process stop <name>");
            commandSender.sendMessage("process start <group-name>");
            commandSender.sendMessage("process list");
            return;
        }

        switch (args[0]) {
            case "stop": {
                if (KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredProxyByName(args[1]) != null) {
                    final ProxyInfo proxyInfo = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredProxyByName(args[1]);
                    KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(proxyInfo.getCloudProcess().getClient(), new PacketOutStopProcess(proxyInfo.getCloudProcess().getName()));
                    commandSender.sendMessage("Trying to stop " + proxyInfo.getCloudProcess().getName() + "...");
                } else if (KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredServerByName(args[1]) != null) {
                    final ServerInfo serverInfo = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredServerByName(args[1]);
                    KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(serverInfo.getCloudProcess().getClient(), new PacketOutStopProcess(serverInfo.getCloudProcess().getName()));
                    commandSender.sendMessage("Trying to stop " + serverInfo.getCloudProcess().getName() + "...");
                } else
                    commandSender.sendMessage("This Server or Proxy is not connected to controller");
                break;
            }
            case "list": {
                commandSender.sendMessage("The following Proxy-Processes are registered: ");
                KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getAllRegisteredProxyProcesses().forEach(e -> commandSender.sendMessage("    - " + e.getCloudProcess().getName() + " | Client=" + e.getCloudProcess().getClient() + " | ID=" + e.getCloudProcess().getProcessID() + " | UID=" + e.getCloudProcess().getProcessUID() + " | Host=" + e.getHost()));
                KlarCloudController.getInstance().getKlarCloudConsoleLogger().emptyLine();
                commandSender.sendMessage("The following CloudServer-Processes are registered: ");
                KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getAllRegisteredServerProcesses().forEach(e -> commandSender.sendMessage("    - " + e.getCloudProcess().getName() + " | Client=" + e.getCloudProcess().getClient() + " | ID=" + e.getCloudProcess().getProcessID() + " | UID=" + e.getCloudProcess().getProcessUID() + " | Host=" + e.getHost()));
                KlarCloudController.getInstance().getKlarCloudConsoleLogger().emptyLine();
                break;
            }
            case "start": {
                if (args.length == 2) {
                    if (KlarCloudController.getInstance().getInternalCloudNetwork().getServerGroups().containsKey(args[1])) {
                        final ServerGroup serverGroup = KlarCloudController.getInstance().getInternalCloudNetwork().getServerGroups().get(args[1]);
                        if (KlarCloudController.getInstance().getChannelHandler().isChannelRegistered(serverGroup.getClient())) {
                            final String id = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().nextFreeServerID(serverGroup.getName());
                            final String name = serverGroup.getName() + KlarCloudController.getInstance().getCloudConfiguration().getSplitter() + (KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getAllRegisteredServerGroupProcesses(serverGroup.getName()).size() < 10 ? "0" : "") + id;
                            KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(serverGroup.getClient(),
                                    new PacketOutStartGameServer(serverGroup, name, UUID.randomUUID(), new Configuration(), id)
                            );
                            commandSender.sendMessage("Trying to startup serverProcess...");
                        } else {
                            commandSender.sendMessage("The Client of the ServerGroup isn't connected to KlarCloudController");
                        }
                    } else if (KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().containsKey(args[1])) {
                        final ProxyGroup proxyGroup = KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().get(args[1]);
                        if (KlarCloudController.getInstance().getChannelHandler().isChannelRegistered(proxyGroup.getClient())) {
                            final String id = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().nextFreeProxyID(proxyGroup.getName());
                            final String name = proxyGroup.getName() + KlarCloudController.getInstance().getCloudConfiguration().getSplitter() + (KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getAllRegisteredServerGroupProcesses(proxyGroup.getName()).size() < 10 ? "0" : "") + id;
                            KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(proxyGroup.getClient(),
                                    new PacketOutStartProxy(proxyGroup, name, UUID.randomUUID(), new Configuration(), id)
                            );
                            commandSender.sendMessage("Trying to startup proxyProcess...");
                        } else {
                            commandSender.sendMessage("The Client of the ProxyGroup isn't connected to KlarCloudController");
                        }
                    } else {
                        commandSender.sendMessage("ServerGroup or ProxyGroup doesn't exists");
                    }
                } else {
                    commandSender.sendMessage("process start <groupName>");
                }
                break;
            }
            default: {
                commandSender.sendMessage("process stop <name>");
                commandSender.sendMessage("process start <group-name>");
                commandSender.sendMessage("process list");
            }
        }
    }

    @Override
    public String getPermission() {
        return "klarcloud.command.process";
    }
}
