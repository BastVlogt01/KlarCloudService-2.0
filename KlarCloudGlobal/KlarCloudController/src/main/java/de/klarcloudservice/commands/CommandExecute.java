/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;
import de.klarcloudservice.netty.packets.PacketOutExecuteCommand;
import de.klarcloudservice.utility.StringUtil;

/**
 * @author _Klaro | Pasqual K. / created on 08.12.2018
 */

public final class CommandExecute implements Command {
    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        if (args.length < 3) {
            commandSender.sendMessage("execute <server/proxy> <name> <command>");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "server": {
                if (KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredServerByName(args[1]) != null) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (short i = 2; i < args.length; i++)
                        stringBuilder.append(args[i]).append(StringUtil.SPACE);

                    KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredServerByName(args[1]).getServerGroup().getClient(), new PacketOutExecuteCommand(stringBuilder.substring(0, stringBuilder.length() - 1), args[0].toLowerCase(), args[1]));
                    commandSender.sendMessage("The command has been executed.");
                } else
                    commandSender.sendMessage("This Server is not connected to controller");
                break;
            }
            case "proxy": {
                if (KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredProxyByName(args[1]) != null) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (short i = 2; i < args.length; i++)
                        stringBuilder.append(args[i]).append(StringUtil.SPACE);

                    KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredProxyByName(args[1]).getProxyGroup().getClient(), new PacketOutExecuteCommand(stringBuilder.substring(0, stringBuilder.length() - 1), args[0].toLowerCase(), args[1]));
                    commandSender.sendMessage("The command has been executed.");
                } else
                    commandSender.sendMessage("This Proxy is not connected to controller");
                break;
            }
            default: {
                commandSender.sendMessage("execute <server/proxy> <name> <command>");
            }
        }
    }

    @Override
    public String getPermission() {
        return "klarcloud.command.execute";
    }
}
