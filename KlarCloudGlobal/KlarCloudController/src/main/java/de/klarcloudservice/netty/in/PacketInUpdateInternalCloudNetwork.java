/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.netty.packets.PacketOutUpdateAll;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

public class PacketInUpdateInternalCloudNetwork implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        KlarCloudController.getInstance().setInternalCloudNetwork(configuration.getValue("networkProperties", TypeTokenAdaptor.getInternalCloudNetworkType()));
        KlarCloudLibraryService.getInstance().setInternalCloudNetwork(KlarCloudController.getInstance().getInternalCloudNetwork());

        KlarCloudController.getInstance().getChannelHandler().sendToAllSynchronized(new PacketOutUpdateAll(KlarCloudController.getInstance().getInternalCloudNetwork()));
    }
}
