/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Arrays;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 28.10.2018
 */

public final class PacketOutStartGameServer extends Packet {
    public PacketOutStartGameServer(final ServerGroup group, final String processName, final UUID serverProcess, final Configuration configuration, final String id) {
        super("StartCloudServer",
                new Configuration().addProperty("group", group).addStringProperty("name", processName).addProperty("serverProcess", serverProcess).addConfigurationProperty("preConfig", configuration).addIntegerProperty("id", Integer.valueOf(id)),
                Arrays.asList(QueryType.COMPLETE, QueryType.NO_RESULT), PacketSender.CONTROLLER);
        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("ServerProcess has been added to queue: " + serverProcess);
    }
}
