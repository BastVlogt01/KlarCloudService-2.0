/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 30.11.2018
 */

public class PacketInCommandExecute implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Player [Name=" + configuration.getStringValue("name") + "/UUID=" + configuration.getValue("uuid", UUID.class) + "] executed on Proxy [Name=" + configuration.getStringValue("proxyName") + "] the command [Name=" + configuration.getStringValue("command") + "]");
    }
}
