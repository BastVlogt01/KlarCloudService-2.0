/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.netty.packets.PacketOutStopProcess;

import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 09.12.2018
 */

public class PacketInRemoveInternalProcess implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        switch (configuration.getStringValue("type").toLowerCase()) {
            case "proxy": {
                final ProxyInfo proxyInfo = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredProxyByUID(configuration.getValue("uid", UUID.class));
                KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(proxyInfo.getProxyGroup().getClient(), new PacketOutStopProcess(proxyInfo.getCloudProcess().getName()));
                break;
            }
            case "server": {
                final ServerInfo serverInfo = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredServerByUID(configuration.getValue("uid", UUID.class));
                KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(serverInfo.getServerGroup().getClient(), new PacketOutStopProcess(serverInfo.getCloudProcess().getName()));
                break;
            }
        }
    }
}
