/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.addons.extendable;

import de.klarcloudservice.addons.JavaAddon;

/**
 * @author _Klaro | Pasqual K. / created on 10.12.2018
 */

public interface ModulePreLoader {
    JavaAddon loadAddon() throws Throwable;
}
