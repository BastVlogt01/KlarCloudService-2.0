/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.web;

import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.utility.cloudsystem.KlarCloudAddresses;
import de.klarcloudservice.web.handler.KlarCloudInternalWebServerHandler;
import de.klarcloudservice.web.utils.WebHandlerAdapter;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import lombok.Getter;

import javax.net.ssl.SSLException;
import java.security.cert.CertificateException;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

@Getter
public class KlarCloudInternalWebServer {
    protected EventLoopGroup bossGroup = KlarCloudLibrary.eventLoopGroup();
    protected EventLoopGroup workerGroup = KlarCloudLibrary.eventLoopGroup();
    private ServerBootstrap serverBootstrap;
    private SslContext sslContext;

    private final WebHandlerAdapter webHandlerAdapter = new WebHandlerAdapter();

    /**
     * Creates a new WebServer Instance
     *
     * @param klarCloudAddresses
     * @param ssl
     * @throws CertificateException
     * @throws SSLException
     * @throws InterruptedException
     */
    public KlarCloudInternalWebServer(KlarCloudAddresses klarCloudAddresses, boolean ssl) throws CertificateException, SSLException, InterruptedException {
        KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Trying to bind WebServer @" + klarCloudAddresses.getHost() + ":" + klarCloudAddresses.getPort() + "...");

        if (ssl) {
            KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Trying to create SelfSignedCertificate...");
            SelfSignedCertificate selfSignedCertificate = new SelfSignedCertificate();
            sslContext = SslContextBuilder.forServer(selfSignedCertificate.key(), selfSignedCertificate.cert()).build();
            KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("SelfSignedCertificate has been created");
        }

        serverBootstrap = new ServerBootstrap()
                .group(bossGroup, workerGroup)

                .childOption(ChannelOption.IP_TOS, 24)
                .childOption(ChannelOption.AUTO_READ, true)
                .childOption(ChannelOption.TCP_NODELAY, true)

                .channel(KlarCloudLibrary.klarCloudServerSocketChanel())

                .childHandler(new ChannelInitializer<Channel>() {
                    @Override
                    protected void initChannel(Channel channel) {
                        if (sslContext != null && ssl)
                            channel.pipeline().addLast(sslContext.newHandler(channel.alloc()));

                        channel.pipeline().addLast(new HttpServerCodec(), new HttpObjectAggregator(Integer.MAX_VALUE), new KlarCloudInternalWebServerHandler(webHandlerAdapter));
                    }
                });
        serverBootstrap.bind(klarCloudAddresses.getHost(), klarCloudAddresses.getPort()).sync().channel().closeFuture();

        KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Successfully bind WebServer @" + klarCloudAddresses.getHost() + ":" + klarCloudAddresses.getPort());
    }

    /**
     * Closes the WebHandler
     */
    public void shutdown() {
        this.bossGroup.shutdownGracefully();
        this.workerGroup.shutdownGracefully();
    }
}
