/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.info;

import de.klarcloudservice.meta.CloudProcess;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 29.10.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class ProxyInfo implements Serializable {
    private static final long serialVersionUID = -3889580749596738985L;

    private CloudProcess cloudProcess;

    private ProxyGroup proxyGroup;

    private String group, host;
    private int port, online, maxMemory;

    @Deprecated
    private boolean maintenance;
    private boolean full;

    private List<UUID> onlinePlayers;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProxyInfo)) return false;
        ProxyInfo proxyInfo = (ProxyInfo) o;
        return getPort() == proxyInfo.getPort() &&
                getOnline() == proxyInfo.getOnline() &&
                getMaxMemory() == proxyInfo.getMaxMemory() &&
                isMaintenance() == proxyInfo.isMaintenance() &&
                isFull() == proxyInfo.isFull() &&
                getCloudProcess().equals(proxyInfo.getCloudProcess()) &&
                getProxyGroup().equals(proxyInfo.getProxyGroup()) &&
                getGroup().equals(proxyInfo.getGroup()) &&
                getHost().equals(proxyInfo.getHost()) &&
                getOnlinePlayers().equals(proxyInfo.getOnlinePlayers());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCloudProcess(), getProxyGroup(), getGroup(), getHost(), getPort(), getOnline(), getMaxMemory(), isMaintenance(), isFull(), getOnlinePlayers());
    }

    @Override
    public String toString() {
        return "ProxyInfo{" +
                "cloudProcess=" + cloudProcess +
                ", proxyGroup=" + proxyGroup +
                ", group='" + group + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", online=" + online +
                ", maxMemory=" + maxMemory +
                ", maintenance=" + maintenance +
                ", full=" + full +
                ", onlinePlayers=" + onlinePlayers +
                '}';
    }
}
