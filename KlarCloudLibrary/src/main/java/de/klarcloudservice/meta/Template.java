/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta;

import de.klarcloudservice.meta.enums.TemplateBackend;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class Template implements Serializable {
    private static final long serialVersionUID = -7937892033580579125L;

    private String name, template_url;
    private TemplateBackend templateBackend;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Template)) return false;
        Template template = (Template) o;
        return getName().equals(template.getName()) &&
                getTemplate_url().equals(template.getTemplate_url()) &&
                getTemplateBackend() == template.getTemplateBackend();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getTemplate_url(), getTemplateBackend());
    }

    @Override
    public String toString() {
        return "Template{" +
                "name='" + name + '\'' +
                ", template_url='" + template_url + '\'' +
                ", templateBackend=" + templateBackend +
                '}';
    }
}
