/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class Client implements Serializable {
    private static final long serialVersionUID = 7702400116714803106L;

    private String name, ip;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return getName().equals(client.getName()) &&
                getIp().equals(client.getIp());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getIp());
    }
}
