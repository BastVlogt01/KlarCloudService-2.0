/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.web;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class InternalWebUser implements Serializable {
    private static final long serialVersionUID = -8467193156656410810L;

    private String name, password;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InternalWebUser)) return false;
        InternalWebUser that = (InternalWebUser) o;
        return getName().equals(that.getName()) &&
                getPassword().equals(that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPassword());
    }

    @Override
    public String toString() {
        return "InternalWebUser{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
