/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.proxy.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class Motd implements Serializable {
    private static final long serialVersionUID = -2392385522979612753L;

    private boolean enabled;
    private String lineOne, lineTwo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Motd)) return false;
        Motd motd = (Motd) o;
        return isEnabled() == motd.isEnabled() &&
                getLineOne().equals(motd.getLineOne()) &&
                getLineTwo().equals(motd.getLineTwo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isEnabled(), getLineOne(), getLineTwo());
    }

    @Override
    public String toString() {
        return "Motd{" +
                "enabled=" + enabled +
                ", lineOne='" + lineOne + '\'' +
                ", lineTwo='" + lineTwo + '\'' +
                '}';
    }
}
