/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.proxy;

import de.klarcloudservice.meta.Template;
import de.klarcloudservice.meta.proxy.configuration.ProxyConfig;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class ProxyGroup implements Serializable {
    private static final long serialVersionUID = 4196459006374952552L;

    protected String name, client;
    protected int startPort, minOnline, maxOnline, memory;

    protected ProxyConfig proxyConfig;
    protected Template template;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProxyGroup)) return false;
        ProxyGroup that = (ProxyGroup) o;
        return getStartPort() == that.getStartPort() &&
                getMinOnline() == that.getMinOnline() &&
                getMaxOnline() == that.getMaxOnline() &&
                getMemory() == that.getMemory() &&
                getName().equals(that.getName()) &&
                getClient().equals(that.getClient()) &&
                getProxyConfig().equals(that.getProxyConfig()) &&
                getTemplate().equals(that.getTemplate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getClient(), getStartPort(), getMinOnline(), getMaxOnline(), getMemory(), getProxyConfig(), getTemplate());
    }

    @Override
    public String toString() {
        return "ProxyGroup{" +
                "name='" + name + '\'' +
                ", client='" + client + '\'' +
                ", startPort=" + startPort +
                ", minOnline=" + minOnline +
                ", maxOnline=" + maxOnline +
                ", memory=" + memory +
                ", proxyConfig=" + proxyConfig +
                ", template=" + template +
                '}';
    }
}
