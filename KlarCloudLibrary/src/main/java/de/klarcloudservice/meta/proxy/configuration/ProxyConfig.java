/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.proxy.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class ProxyConfig implements Serializable {
    private static final long serialVersionUID = 8806439752733227333L;

    private boolean maintenance, viaVersion, controllerCommandLogging;
    private Motd normalLayout;
    private int maxPlayers;
    private String ip;
    private Collection<String> whitelist;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProxyConfig)) return false;
        ProxyConfig that = (ProxyConfig) o;
        return isMaintenance() == that.isMaintenance() &&
                isViaVersion() == that.isViaVersion() &&
                isControllerCommandLogging() == that.isControllerCommandLogging() &&
                getMaxPlayers() == that.getMaxPlayers() &&
                getNormalLayout().equals(that.getNormalLayout()) &&
                getIp().equals(that.getIp()) &&
                getWhitelist().equals(that.getWhitelist());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isMaintenance(), isViaVersion(), isControllerCommandLogging(), getNormalLayout(), getMaxPlayers(), getIp(), getWhitelist());
    }

    @Override
    public String toString() {
        return "ProxyConfig{" +
                "maintenance=" + maintenance +
                ", viaVersion=" + viaVersion +
                ", controllerCommandLogging=" + controllerCommandLogging +
                ", normalLayout=" + normalLayout +
                ", maxPlayers=" + maxPlayers +
                ", ip='" + ip + '\'' +
                ", whitelist=" + whitelist +
                '}';
    }
}
