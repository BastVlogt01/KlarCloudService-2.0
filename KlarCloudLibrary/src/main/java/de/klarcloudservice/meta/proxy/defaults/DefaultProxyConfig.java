/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.proxy.defaults;

import de.klarcloudservice.meta.proxy.configuration.Motd;
import de.klarcloudservice.meta.proxy.configuration.ProxyConfig;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

final class DefaultProxyConfig extends ProxyConfig implements Serializable {
    private static final long serialVersionUID = 5559609589811160975L;

    DefaultProxyConfig(String host) {
        super(true, true, false,
                new Motd(true, "   §c§lK§4§llar§c§lC§4§lloud§c§lS§4§lervice§8│ §7An §4intelligent §7CloudSystem", "    §8➥ §7Version §8» §4%version%§8│ §7Proxy §8» §4%proxy%"),
                512, host,
                Arrays.asList("_Klaro", "HerrSammy", "MrCodingMen")
        );
    }
}
