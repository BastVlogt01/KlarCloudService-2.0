/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.proxy.defaults;

import de.klarcloudservice.meta.Template;
import de.klarcloudservice.meta.enums.TemplateBackend;
import de.klarcloudservice.meta.proxy.ProxyGroup;

import java.io.Serializable;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

public class DefaultProxyGroup extends ProxyGroup implements Serializable {
    private static final long serialVersionUID = -1867335836689571544L;

    @Override
    public String toString() {
        return "DefaultProxyGroup{}";
    }

    public DefaultProxyGroup(final String host) {
        super("Proxy", "Client-01", 25565, 1, -1, 128, new DefaultProxyConfig(host),
                new Template("client", null, TemplateBackend.CLIENT));
    }

    public DefaultProxyGroup(final String name, final String client, final String host) {
        super(name, client, 25565, 1, -1, 128, new DefaultProxyConfig(host),
                new Template("default", null, TemplateBackend.CLIENT));
    }
}
