/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.server.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author _Klaro | Pasqual K. / created on 01.11.2018
 */

@AllArgsConstructor
@Getter
public class AdvancedConfiguration implements Serializable {
    private static final long serialVersionUID = -976102679117872951L;

    protected boolean disableNether, enableCommandBlock, spawnMonsters, spawnAnimals, PVP, hardcore, generateStructures, fly, query, announcePlayerAchievements, forceGamemode;
    protected String worldName, resourcePack;
    protected int maxPlayers, viewDistance, idleTimeout, difficulty, defaultGamemode, maxBuildHeight;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdvancedConfiguration)) return false;
        AdvancedConfiguration that = (AdvancedConfiguration) o;
        return isDisableNether() == that.isDisableNether() &&
                isEnableCommandBlock() == that.isEnableCommandBlock() &&
                isSpawnMonsters() == that.isSpawnMonsters() &&
                isSpawnAnimals() == that.isSpawnAnimals() &&
                isPVP() == that.isPVP() &&
                isHardcore() == that.isHardcore() &&
                isGenerateStructures() == that.isGenerateStructures() &&
                isFly() == that.isFly() &&
                isQuery() == that.isQuery() &&
                isAnnouncePlayerAchievements() == that.isAnnouncePlayerAchievements() &&
                isForceGamemode() == that.isForceGamemode() &&
                getMaxPlayers() == that.getMaxPlayers() &&
                getViewDistance() == that.getViewDistance() &&
                getIdleTimeout() == that.getIdleTimeout() &&
                getDifficulty() == that.getDifficulty() &&
                getDefaultGamemode() == that.getDefaultGamemode() &&
                getMaxBuildHeight() == that.getMaxBuildHeight() &&
                getWorldName().equals(that.getWorldName()) &&
                Objects.equals(getResourcePack(), that.getResourcePack());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isDisableNether(), isEnableCommandBlock(), isSpawnMonsters(), isSpawnAnimals(), isPVP(), isHardcore(), isGenerateStructures(), isFly(), isQuery(), isAnnouncePlayerAchievements(), isForceGamemode(), getWorldName(), getResourcePack(), getMaxPlayers(), getViewDistance(), getIdleTimeout(), getDifficulty(), getDefaultGamemode(), getMaxBuildHeight());
    }

    @Override
    public String toString() {
        return "AdvancedConfiguration{" +
                "disableNether=" + disableNether +
                ", enableCommandBlock=" + enableCommandBlock +
                ", spawnMonsters=" + spawnMonsters +
                ", spawnAnimals=" + spawnAnimals +
                ", PVP=" + PVP +
                ", hardcore=" + hardcore +
                ", generateStructures=" + generateStructures +
                ", fly=" + fly +
                ", query=" + query +
                ", announcePlayerAchievements=" + announcePlayerAchievements +
                ", forceGamemode=" + forceGamemode +
                ", worldName='" + worldName + '\'' +
                ", resourcePack='" + resourcePack + '\'' +
                ", maxPlayers=" + maxPlayers +
                ", viewDistance=" + viewDistance +
                ", idleTimeout=" + idleTimeout +
                ", difficulty=" + difficulty +
                ", defaultGamemode=" + defaultGamemode +
                ", maxBuildHeight=" + maxBuildHeight +
                '}';
    }
}
