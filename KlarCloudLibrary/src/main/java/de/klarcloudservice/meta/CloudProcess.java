/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 29.10.2018
 */

@AllArgsConstructor
@Getter
@Setter
public class CloudProcess implements Serializable {
    private static final long serialVersionUID = -532002825303576279L;

    private String name;
    private UUID processUID;
    private String client;
    private int processID;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CloudProcess)) return false;
        CloudProcess that = (CloudProcess) o;
        return getProcessID() == that.getProcessID() &&
                getName().equals(that.getName()) &&
                getProcessUID().equals(that.getProcessUID()) &&
                getClient().equals(that.getClient());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getProcessUID(), getClient(), getProcessID());
    }

    @Override
    public String toString() {
        return "CloudProcess{" +
                "name='" + name + '\'' +
                ", processUID=" + processUID +
                ", client='" + client + '\'' +
                ", processID=" + processID +
                '}';
    }
}
