/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.startup;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 29.10.2018
 */

@AllArgsConstructor
@Getter
public class ProxyStartupInfo implements Serializable {
    private static final long serialVersionUID = -295123813122792999L;

    private UUID uid;
    private String name;
    private ProxyGroup proxyGroup;
    private Configuration configuration;
    private int id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProxyStartupInfo)) return false;
        ProxyStartupInfo that = (ProxyStartupInfo) o;
        return getUid().equals(that.getUid()) &&
                getName().equals(that.getName()) &&
                getProxyGroup().equals(that.getProxyGroup()) &&
                getConfiguration().equals(that.getConfiguration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUid(), getName(), getProxyGroup(), getConfiguration());
    }

    @Override
    public String toString() {
        return "ProxyStartupInfo{" +
                "uid=" + uid +
                ", name='" + name + '\'' +
                ", proxyGroup=" + proxyGroup +
                ", configuration=" + configuration +
                '}';
    }
}
