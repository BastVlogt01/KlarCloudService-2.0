/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.enums;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

public enum GroupResetType {
    LOBBY,
    NO_RESET,
    RESET;

    GroupResetType() {
    }
}
