/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.signs;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * @author _Klaro | Pasqual K. / created on 11.12.2018
 */

@EqualsAndHashCode
@Getter
@AllArgsConstructor
public class SignLayout {
    private String[] lines;

    @Getter
    @AllArgsConstructor
    @EqualsAndHashCode
    public static class LoadingLayout {
        private int perSecondAnimation;
        private transient int currentAnimation;
        private SignLayout[] layouts;

        public SignLayout getNextLayout() {
            currentAnimation++;
            if (currentAnimation >= layouts.length)
                currentAnimation = 0;
            return layouts[currentAnimation];
        }
    }

    @Getter
    @AllArgsConstructor
    @EqualsAndHashCode
    public static class GroupLayout {
        private SignLayout maintenanceLayout, emptyLayout, fullLayout, onlineLayout;
    }
}
